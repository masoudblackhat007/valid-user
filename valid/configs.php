<?php
namespace valid;

return[
    "exist.name" =>"4041",
    "exist.password" => "4042",
    "exist.confirm_password" => "4043",
    "exist.email" => "4044",
    "required.name"=>"2041",
    "required.password" =>"2042",
    "required.confirm_password" =>"2043",
    "required.email" =>"2044",
    "max.password" =>"240052",
    "equal.password" =>"240053",
    "valid.email" =>"240054",
    "attempt.security" => "4001",
    "csrf.security"=> "4002",
    "exist.session_csrf"=>"4003",
    "exist.session_decay_attempt_time"=>"4004",
    "block.security"=>"4005",
    "csrf.empty"=>"4005",
    "required_name" => "نام کاربری نمی تواند خالی باشد",
    "required_password"=>"رمز عبور نمی تواند خالی باشد",
    "max_password"=>"رمز عبور ضعیف است",
    "block_security"=>'block',
    "csrf_security"=>"خطای ناشناخته ای رخ داده لطفا مرورگر را بسته و مجدد تلاش نمایید",
    "csrf_empty"=>"خطای ناشناخته ای رخ داده لطفا مرورگر را بسته و مجدد تلاش نمایید",
    "required_confirm_password"=>"تکرار پسورد وارد نشده است",
    "required_email"=>"ایمیل نمی تواند خالی باشد",
    "equal_password"=>"رمز عبور و تکرار رمز عبور یکسان نمی باشد",
    "valid_email"=>"ایمیل وارد شده صحیح نمی باشد",
];
