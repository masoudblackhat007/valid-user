<?php

namespace valid;

use Factory;

//class CheckValid
//{
//
//    private Factory $factory;
//    use Helper;
//
//    public function __construct(Factory $factory){
//        $this->factory = $factory;
//    }
//
//    //design-patterns builder and factory
////    public function render(string $item, $request, int $max_attempt = 20, $timeBlock = 5){
////        $this->sessionStart();
////        $errors = array();
////        if ($item === "login"){
////            $userValidation = $this->get_validation($request);
////            $userSecurity = $this->get_security($request);
////            $errors['security'] = $userSecurity
////                ->check_csrf_token()->block_user($timeBlock, $max_attempt)
////                ->get_errors();
////
////            $errors['VALID']  = $userValidation
////                ->valid_field_name()->valid_field_password()
////                ->required_name()->required_password()->max_password()
////                ->get_errors();
////
////        }else if ($item === 'register'){
////            $userValidation = $this->get_validation($request);
////            $userSecurity = $this->get_security($request);
////
////            $errors['security'] = $userSecurity
////                ->check_csrf_token()->block_user($timeBlock, $max_attempt)
////                ->get_errors();
////
////            $errors['VALID'] = $userValidation
////                ->valid_field_name()->required_name()->valid_field_password()
////                ->required_password()->max_password()->valid_field_confirm_password()
////                ->required_confirm_password()->equal_password()->valid_field_email()
////                ->required_email()->validate_email()->get_errors();
////        }
////        return $this->factory->handel_errors()->handel($errors)->view($request);
////
////    }
//
////    public function checkBlock(): bool{
////        $this->sessionStart();
////        if (isset($_SESSION['blockUser']) && $_SESSION['blockUser']){
////            return $this->decayUserPerTime();
////        }
////        return true;
////
////    }
//}

/*
 * adapter design pattern
 */
interface ValidationUser{
    public function valid(array $request, int $max_attempt = 20, $timeBlock = 5);
}

class ValidLogin implements ValidationUser
{
    use Helper;
    private object $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function valid( array $request, int $max_attempt = 20, $timeBlock = 5):string{
        // TODO: Implement valid() method.
        $this->sessionStart();
        $errors = array();
        $userValidation = $this->get_validation($request);
        $userSecurity = $this->get_security($request);

        $errors['security'] = $userSecurity
            ->check_csrf_token()->block_user($timeBlock, $max_attempt)
            ->get_errors();

        $errors['VALID'] = $userValidation
            ->valid_field_name()->valid_field_password()
            ->required_name()->required_password()->max_password()
            ->get_errors();

        return $this->factory->handel_errors()->handel($errors)->view($request);
    }

}

class ValidRegister implements ValidationUser
{
    use Helper;
    private object $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function valid(array $request, int $max_attempt = 20, $timeBlock = 5){
        // TODO: Implement valid() method.
        $userValidation = $this->get_validation($request);
        $userSecurity = $this->get_security($request);

        $errors['security'] = $userSecurity
            ->check_csrf_token()->block_user($timeBlock, $max_attempt)
            ->get_errors();

        $errors['VALID'] = $userValidation
            ->valid_field_name()->required_name()->valid_field_password()
            ->required_password()->max_password()->valid_field_confirm_password()
            ->required_confirm_password()->equal_password()->valid_field_email()
            ->required_email()->validate_email()->get_errors();

        return $this->factory->handel_errors()->handel($errors)->view($request);
    }
}

function clientCode(ValidationUser $validation, array $request, int $max_attempt = 20, $timeBlock = 5):string
{
    return $validation->valid($request, $max_attempt, $timeBlock);
}
