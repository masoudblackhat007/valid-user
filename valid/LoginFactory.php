<?php

namespace valid;

use Factory;

class LoginFactory implements Factory
{

    public function user_validation(array $request): Event{
        // TODO: Implement user_validation() method.
        return new UserValidation($request);
    }

    public function user_security(array $request): Security{
        // TODO: Implement user_security() method.
        return new UserSecurity($request);
    }

    public function handel_errors() :ManagerEr{
        // TODO: Implement handel_errors() method.
        return new HandelError();
    }

}