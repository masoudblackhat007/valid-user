<?php

namespace valid;

class UserSecurity implements Security
{

    use Helper;
    private array $request;
    private array $errors ;

    public function __construct(array $request){
        $this->request = $request;
    }

    public function check_csrf_token(): Security{
        // TODO: Implement checkCSRFToken() method.
        $this->sessionStart();
        if ($this->exist_csrf() && $this->exist_session_csrf()){
            if ($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] !== $this->request['csrf']) {
                $this->errors['csrf']['security'] = true;
            }else{
                $this->errors['csrf']['security'] = false;
            }
        }else{
            $this->errors['exist']['session_csrf'] = true;
        }

        return  $this;
    }

    public function get_errors(): array{
        // TODO: Implement get_errors() method.
        return $this->errors;
    }

    private function exist_session_csrf(): bool {
        // TODO: Implement exist_session() method.
        if (isset($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'])){
            return true;
        }
        return false;
    }

    private function exist_csrf(): bool{
        //TODO: Implement exist_csrf() method.
        if (isset($this->request['csrf'])){
            return true;
        }
        return false;
    }

    public function block_user($time, $max_attempt):Security{
        $this->sessionStart();
        if ($this->attempt($max_attempt)) {
            $this->block_user_per_time($time);
            if (!$this->decayUserPerTime()) {
                $this->errors['block']['security'] = true;
                $_SESSION['blockUser'] = true;
            }else{
                $this->errors['block']['security'] = false;
                $_SESSION['blockUser'] = false;
            }
        }else{
            $this->errors['block']['security'] = false;
            $_SESSION['blockUser'] = false;
        }
        return $this;
    }
}