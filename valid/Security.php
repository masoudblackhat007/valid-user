<?php
namespace valid;
interface Security
{

//    public function attempt($attempt_max):Security;

    public function csrf_token():string;

    public function check_csrf_token(): Security;

    public function get_errors():array;

    public function block_user($time, $max_attempt):Security;



}